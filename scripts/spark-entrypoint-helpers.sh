#!/bin/sh

# from docker-hadoop-base/scripts/application-helpers.sh
. $(dirname "${0}")/application-helpers.sh

# For Spark properties, see
# https://spark.apache.org/docs/latest/configuration.html
# https://spark.apache.org/docs/latest/monitoring.html#spark-configuration-options

set_webui_port() {
	if [ -n "${PROP_SPARK_spark_ui_port}" ]; then
		export WEBUI_PORT="${PROP_SPARK_spark_ui_port}"
	elif [ -n "${WEBUI_PORT}" ]; then
		export PROP_SPARK_spark_ui_port="${WEBUI_PORT}"
	fi
}

set_histui_port() {
	if [ -n "${PROP_SPARK_spark_history_ui_port}" ]; then
		export HISTUI_PORT="${PROP_SPARK_spark_history_ui_port}"
	elif [ -n "${PROP_SPARK_spark_yarn_historyServer_address}" ]; then
		export HISTUI_PORT="${PROP_SPARK_spark_yarn_historyServer_address##*:}"
	elif [ -z "${HISTUI_PORT}" ]; then
		export HISTUI_PORT="18080"
	fi
}

set_log_dir() {
	if [ -n "${PROP_SPARK_spark_history_fs_logDirectory}" ]; then
		export LOG_DIR="${PROP_SPARK_spark_history_fs_logDirectory}"
	elif [ -n "${PROP_SPARK_spark_eventLog_dir}" ]; then
		export LOG_DIR="${PROP_SPARK_spark_eventLog_dir}"
	fi
	if [ -n "${LOG_DIR}" ]; then
		export PROP_SPARK_spark_eventLog_dir="${LOG_DIR}"
		export PROP_SPARK_spark_history_fs_logDirectory="${LOG_DIR}"
		if [ -n "${HISTUI_PORT}" ]; then
			export PROP_SPARK_spark_history_ui_port="${HISTUI_PORT}"
			export PROP_SPARK_spark_eventLog_enabled="true"
			[ -z "${PROP_SPARK_spark_yarn_historyServer_address}" ] \
			&& export PROP_SPARK_spark_yarn_historyServer_address="\${hadoopconf-yarn.resourcemanager.hostname}:${PROP_SPARK_spark_history_ui_port}"
		fi
	fi
}

expand_spark_jars_dirs() {
	local result
	for I in $(echo "${PROP_SPARK_spark_jars}" | tr ',' ' '); do
		if [ -d "${I}" ]; then
			result="${result},$(ls -d ${I}/*.jar | tr '\n' ',' | sed 's/,$//')"
		else
			result="${result},${I}"
		fi
	done
	export PROP_SPARK_spark_jars="${result#,}"
}

# make HDFS log directory for Spark History server
make_log_dir() {
	if [ -z "${LOG_DIR##hdfs://*}" ]; then
		# make_hdfs_dir is from docker-hadoop-base/scripts/application-helpers.sh
		make_hdfs_dir "${LOG_DIR}?user=${SPARK_USER}&group=${SPARK_USER}&perms=1777"
	fi
}
