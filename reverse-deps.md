# Reverse Dependencies

After rebuild of this Docker image, it is necessary to rebuild also the following Docker images:

*	docker-livy
*	docker-spark-app
*	docker-zeppelin
